# Creation of AWS infrastructure (EC2, SNS topic, CloudWatch components - metric filters, alarms, dashboard) with CloudFormation

My post on Medium about the project: https://medium.com/p/b4347a6ee6e2

## Pre-reqs

Before you start, make sure the following requirements are met:
- An AWS account with permissions to create resources.
- AWS CLI installed on your local machine.

## Building & Running 

1. Create an S3 bucket for nested stack templates

on Linux:
```date=$(date +%Y%m%d%H%M%S)```

on Windows PowerShell:
```$date = Get-Date -Format "yyyyMMddHHmmss"```

```
aws s3api create-bucket --bucket cloudformation-templates-${date} --region YOUR_REGION \
  --create-bucket-configuration LocationConstraint=YOUR_REGION
```

2. Send all nested stack files to the S3 bucket

```
aws s3 cp . s3://cloudformation-templates-${date} --recursive --exclude ".git/*" --exclude README.md --exclude user_data.txt
```

3. Add policy to the S3 bucket for access from EC2 instance

```
aws s3api put-bucket-policy --bucket cloudformation-templates-${date} --policy '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"Service":"ec2.amazonaws.com"},"Action":"s3:GetObject","Resource":"arn:aws:s3:::'"cloudformation-templates-${date}"'/*"}]}'
```

4. Fill in all necessary Parameters in root.yaml and the bucket name in user_data.txt files and send all nested stack files to the S3 bucket

```
aws s3 cp . s3://cloudformation-templates-${date} --recursive --exclude ".git/*" \
  --exclude README.md --exclude user_data.txt --exclude ".images/*"
```

5. Create a CloudFormation stack

```
aws cloudformation create-stack \
    --stack-name monitoring-ws-cloudwatch \
    --template-body file://root.yaml \
    --capabilities CAPABILITY_NAMED_IAM \
    --parameters ParameterKey=UserData,ParameterValue="$(base64 -i user_data.txt)" \
    --capabilities CAPABILITY_NAMED_IAM \
    --disable-rollback
```

5. Open your mailbox and confirm your subscription from the SNS topic.

Go to AWS Console -> CloudWatch and see if Log group, Metric filters, Alarms, and Dashboard exist. You need to wait some period for new log data from the EC2 instance. A simple PowerShell script that makes a copy of the AmazonCloudWatchAgent.msi in the same folder every second was created for the test.

Access to the EC2 instance is possible through the Systems Manager. Go to AWS console -> AWS Systems Manager -> Fleet Manager -> choose the created EC2 instance -> Node actions -> Connect -> Start terminal session. Here you can check if everything was created and configured correctly. 

6. Delete the CloudFormation stack

```
aws cloudformation delete-stack --stack-name monitoring-ws-cloudwatch
```

7. Delete all files from the S3 bucket and the S3 bucket

```
aws s3 rm s3://cloudformation-templates-${date} --recursive
```

```
aws s3 rb s3://cloudformation-templates-${date} --force
``` 

## Infrastructure schema

![schema](images/steps_cloudwatch_disk_space.png)

You can support me with a virtual coffee https://www.buymeacoffee.com/andrworld1500 .